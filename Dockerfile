FROM debian:bookworm

ARG APP_UID=2000

ARG APP_GID=2000

LABEL maintainer="TANGO Controls team <contact@tango-controls.org>"

ENV DEBIAN_FRONTEND=noninteractive
ENV CMAKE_PREFIX_PATH=/home/tango/lib/cmake
ENV PKG_CONFIG_PATH=/home/tango/lib/pkgconfig

RUN apt-get update -qq &&                     \
  apt-get install -y --no-install-recommends  \
  apt-utils                                   \
  build-essential                             \
  bzip2                                       \
  ca-certificates                             \
  ca-certificates-java                        \
  cmake                                       \
  cppzmq-dev                                  \
  curl                                        \
  git                                         \
  libc-ares-dev                               \
  libcurl4-openssl-dev                        \
  libjpeg-dev                                 \
  libmariadb-dev                              \
  libre2-dev                                  \
  libssl-dev                                  \
  libzmq3-dev                                 \
  libzstd-dev                                 \
  mariadb-server                              \
  ninja-build                                 \
  openjdk-17-jre-headless                     \
  pkg-config                                  \
  python3                                     \
  python3-dev                                 \
  sudo                                        \
  zlib1g-dev                               && \
  rm -rf /var/lib/apt/lists/*              && \
  service mariadb restart                  && \
  mysql -u root -e "SET PASSWORD = PASSWORD('secret');"

RUN groupadd -g "$APP_GID" tango                               && \
    useradd -l -u "$APP_UID" -g "$APP_GID" -ms /bin/bash tango && \
    usermod -a -G sudo tango                                   && \
    echo "%sudo ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers        && \
    echo "/home/tango/lib" > /etc/ld.so.conf.d/home.conf

USER tango

WORKDIR /home/tango

COPY --chown="$APP_UID":"$APP_GID" scripts/*.sh ./

RUN ./common_setup.sh               && \
    ./install_omniorb.sh            && \
    ./install_opentelemetry_deps.sh && \
    ./install_opentelemetry.sh      && \
    rm -rf dependencies
